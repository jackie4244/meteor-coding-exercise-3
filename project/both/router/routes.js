/*****************************************************************************/
/* Client and Server Routes */
/*****************************************************************************/
Router.configure({
  layoutTemplate: 'MasterLayout',
  loadingTemplate: 'Loading',
  notFoundTemplate: 'NotFound'
});

Router.route('/', {name: 'home'});
Router.route('/scenarios', {name: 'scenarios'});
Router.route('/npcharacter', {name: 'npcharacter'});
