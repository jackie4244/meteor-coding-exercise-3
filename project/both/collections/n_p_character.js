NPCharacter = new Mongo.Collection('n_p_character');

NPCharacter.attachSchema(new SimpleSchema(
  {

  title:
  {
    type:String
  },
  npCharacterRequestIds:
  {
    type:[String],
    optional: true
  }, 
  
  npCharacterRequestMoney:
  {
    type:[Number],
    min: 0
  },
  npCharacterRequestHealth:
  {
    type:[Number],
    min: 0
  }
}));